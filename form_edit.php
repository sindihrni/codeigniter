<html>
<head><title>Halaman Form Edit</title></head>
<body>
	<h3>Form Edit Data</h3>

	<table>
		<form action="<?php echo base_url('Welcome/AksiEdit') ?>" method="post">
		<tr>
			<td>NIM</td>
			<td>:</td>
			<td>
				<input type="text" value="<?php echo $data_mhs->nim ?>" disabled>
				<input type="hidden" name="nim">
			</td>
		</tr>
		<tr>
			<td>NAMA</td>
			<td>:</td>
			<td><input type="text" name="nama" value="<?php echo $data_mhs->nama ?>"></td>
		</tr>
		<tr>
			<td>JURUSAN</td>
			<td>:</td>
			<td><input type="text" name="jurusan" value="<?php echo $data_mhs->jurusan ?>"></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="SIMPAN"></td>
		</tr>
	</table>
</body>
</html>