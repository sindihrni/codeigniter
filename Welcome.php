<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

   public function __construct(){
    parent::__construct();
    //$this->load->helper('url'); // Memuat helper URL
    $this->load->model('M_Mahasiswa');
}


    public function index()
    {
        // Mengambil data mahasiswa dari model
        $data['data_mhs'] = $this->M_Mahasiswa->getDataMahasiswa();

        // Memuat view 'home' dan mengirimkan data mahasiswa ke dalamnya
        $this->load->view('home', $data);
    }

    public function formInput()
    {
        $this->load->view('form_input');
    }

    public function formEdit($id)
    {
        $recordMhs = $this->M_Mahasiswa->getDataMahasiswaDetail($id);
        $data['data_mhs'] = $recordMhs;
        $this->load->view('form_edit', $data);
    }

    public function AksiInsert()
    {
        // Mengambil data dari form
        $nim = $this->input->post('nim');
        $nama = $this->input->post('nama');
        $jurusan = $this->input->post('jurusan');

        // Menyiapkan data untuk dimasukkan ke dalam database
        $dataInsert = array(
            'nim' => $nim,
            'nama' => $nama,
            'jurusan' => $jurusan,
        );

        // Memanggil model untuk memasukkan data mahasiswa
        $this->M_Mahasiswa->InsertDataMhs($dataInsert);

        // Mengalihkan pengguna kembali ke halaman utama
        redirect(base_url());
    }

    public function AksiEdit()
    {
        // Mengambil data dari form
        $nim = $this->input->post('nim');
        $nama = $this->input->post('nama');
        $jurusan = $this->input->post('jurusan');

        // Menyiapkan data untuk dimasukkan ke dalam database
        $dataUpdate = array(
            'nama' => $nama,
            'jurusan' => $jurusan,
        );

        // Memanggil model untuk memperbarui data mahasiswa
        $this->M_Mahasiswa->EditDataMhs($dataUpdate, $nim);

        // Mengalihkan pengguna kembali ke halaman utama
        redirect(base_url());
    }
}
